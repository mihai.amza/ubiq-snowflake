import ubiq.fpe as fpe
import json

import requests
import http
from typing import Dict, Any, Tuple

# from ubiq import http_auth
# [API Key-short]
# ACCESS_KEY_ID = gRavZbqujOgTCR/QKjh/HZA8
# SECRET_SIGNING_KEY = eBTJlNj3oxEfI/+XJdD4TaMEG7rXd+DzhEQrpIoLNBcM
# SECRET_CRYPTO_ACCESS_KEY = qY8M1xkH9h8FWYmIFLHc8Po408I3wvrGuCTikv1zdKb5
# SERVER=http://localhost:8083

# 6EYWUsTOi1YFXgslMIqFn7+5
# WJCigfi/c63ncnNFyF7i1eFeU5tYA+ukWSZg8NvUtRtx
# LamjC19/C1t0ZGdlnt9PclRGefC1o2zNlJfzSC/5bwDr


host = "https://api.ubiqsecurity.com/api/v0/"
papi = '6EYWUsTOi1YFXgslMIqFn7+5'
sapi = 'WJCigfi/c63ncnNFyF7i1eFeU5tYA+ukWSZg8NvUtRtx'
ffs_name = 'SSN'

def fmtInput(s: str, pth: str, ics: str, ocs: str) -> Tuple[str, str]:
    fmt = ''
    trm = ''
    for c in s:
        if c in pth:
            fmt += c
        else:
            fmt += ocs[0]
            if c in ics:
                trm += c
            else:
                raise RuntimeError("invalid input character '%s' (valid characters: %s | valid passthrough characters: %s)" %(c, ics, pth))
    return fmt, trm

# def get_key_at_num(n, ffs):
#     url = host + 'fpe/key'
#     url += '?ffs_name=' + ffs
#     url += '&papi=' + papi
#     if n >= 0:
#         url += '&key_number=' + str(n)
#     # print(url)
#     resp = requests.get(url, auth=http_auth(papi, sapi))
#     if resp.status_code != http.HTTPStatus.OK:
#         raise 'oops'
#     resp = json.loads(resp.content.decode())
#     # print(resp)
#     return resp['wrapped_data_key']

# for i in range(4,9):
#     print(get_key_at_num(i, ffs_name))

cache = {
    "SSN": {
        "ffs": {
            "encryption_algorithm": "FF1",
            "input_character_set": "0123456789",
            "msb_encoding_bits": 0,
            "name": "SSN",
            "output_character_set": "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "passthrough": "-",
            "tweak": "C48Ss7QUzt6wx8t39qg3rRWDp0CQcWmAlAfUO7AhC/Q=",
            "tweak_max_len": 32,
            "tweak_min_len": 6
        },
        "encrypted_private_key": "-----BEGIN ENCRYPTED PRIVATE KEY-----\r\nMIIJnzBJBgkqhkiG9w0BBQ0wPDAbBgkqhkiG9w0BBQwwDgQIaCkVHc7efHsCAggA\r\nMB0GCWCGSAFlAwQBKgQQn7cHUo97aZrM1koQKpjRYASCCVA5wUrUXVGBnNIl3rnR\r\nepMJv7YVcnjJO758GXCypvNK9dBqBsOazE5GVSoU+F0+ULPpC5n/QJdREG4bzv0t\r\nDy9esmJnThd2UAGf3eK6WYeGIkYkCtFdzVz9ELJ+QC46kmhxIcikO5JLkE95V3Ir\r\njfL3u2HsL+sUutluAtFFgmtFRxwv23SO5Dfbl22v+nhz8Aycf9e0xIR2dnMFmgs+\r\nrF2KB/OZgt4vzNYvlqUmp61R7OJJUp29Gb73TPTDolRTDZuP7rSL4zYU4t+eFdvZ\r\nd8iCzIHpIo4biHj3mz9M+5PDZD3B6Zt4r3GWCCaRdbSV+ZEfLbuLK4nF84J42gGG\r\nZYQxulknjqPiuZewN4ke9c9PPgTv0wVQKn2CzwQy/Kdu7CfGFTqBf2E+4llAdgzX\r\nTcOpddC4a38pu8YCsd+LNiAj/fL5JapUSNAKHCP4+YbBdJ55qmXb38K9JjF298ZF\r\nQCQJR5s4J0BAcq29TTgg/kT19Es1Vc0h30AbSRX8CowAcWl1hJra7OtnxCFiTEuj\r\njSZJl+q+uYMNxHX5L8jpScBTqrP/mwVMm/dL3Djlh2zzTd/lBDGyDRSffHQz0lHR\r\nbSIqg277W9zj15og+n0RYYGA0z+u/KY2dVbbuLrx1dG5YUZPCJRWJ9dWKYlyinvY\r\nJBFVuNrTBwr57UpHi3iLubaCaggCQxI0zcV1kYMoAJjbTuuPFH/J2WKROUGQOf6h\r\nAWruUuFfj/v/NIxOAWTFpfmTB6drKvsSplkoQd7g5bgUEOdfMR7W3q5F6oJyoE11\r\nPtFgU9MF5kkzeMZ3HjEwAHiOe6YuKKiQ4nBdpvy4A5MfYBLaGQLYLUUmHNqZ0exO\r\nHzdNMAdVjYQlIjOBAvA+HE8dUzKTxuRhnquaT7o6wwDE1hI4Bylqmqa8XhSSmxtJ\r\n2DkO+NgvUvi7Lec5qElN0EpvshPbPepxWLkgpAEEXPkI0NjcnYPs7XyQJ//9jGhs\r\n6UW8hC9R/yaqf3bL6Xc4zwJkSC4ji/iuy0BTBhvMXWvmb5tbGUG6V3N2qPdkEYsa\r\nj9UlBScBDoZmPeuZTqt4k+2KP68fqnmeUTokj7as+SnqAGT8Fh9N/vdIFKQLNbqq\r\n1IPJ7uzD4x4nh+V1v0syjIJ6yACIejlmw7rRWLuuH5fczOx9x4y7k/maovfNxSzJ\r\nQwC6u0ij0reSbK2rYq5jeshqB9H8zA38f92OYJAde4K8u6hrGObXPP4I7ohpm4bX\r\nyDjZQctESyMvYFeeGp6xFGtv0MGmoFgCU1pRukVSqs/Cr9IQsZzxuiVgU/5uhgCu\r\nyGV2Vf8xh+wuFnwAIMpnxJZWTc7m2HlYbzbO9lZVcE/DOHFpBrK/U90msSZSwoFU\r\nN6ydjLql3D3vjQREPYtU7stk/BzH4y5Vz92g+z2syQhXnxNclzchdHlVHK7bxHeJ\r\nH8/EBEOfvFbCddCH7P2tiF7oUr/9LmoiJs4AybZpHuFqBPXul7EyymASZ0z8pbd3\r\nux3C9LrYVhgVNH5hPXWQtcY4EsBqvOtS4FHBvjhJmY83+Gfa1MUNNXHppW/iBJRm\r\nWd0IZ/YxbhFDzicNwaUKq97RRmP4QRUrfBe4SxJXCNUGpaD203Y0YPHuhVIps/Nm\r\nFd04VZjy1fuWIJjs4OFWTWwKPSvna7i3HepXERvsDwJmQHi7VPxbFOxGEJzNCHU9\r\n/lWY+5V6rwG12DoniDWsvmz2U8kFOwhY2wRfGcXSPoj+MdzBKBpYP0H20sy+i3BQ\r\nSuCOVSp7D+ET7nILhJGXPIPPXV0JXH9L6TU3TlhhD+gziz2UyZkAE+Orj4qJsG9H\r\nk22z59UIZyNiaImhYvLRC9H1dLKa+OY1yer1JBbaOkuTGblkDKiEmRBVZR37bcIA\r\nbrh73o9etKmV+Q83cz1HNDfD+PageCTMjb5dbyX7YpuowUKiWgV7ev/tIvzhoSRa\r\nmWVEB51u9ABqbGTfzJ3WamhWvhdw5ioIjampSGkczm73jdMLE2XrzzEuySqp5Eou\r\nV8J6joICmeRzkn3yinXnU5wlAN+9Uwx8RUbxxgGeU9GYoGadVWFqGWAS5MvDxU1v\r\nGHLdWT+PWG+eSvNWgxLzowOmBpf/nj8FnqjCAyuxLcltQld4U5ULhXqwRUlK2pUe\r\nO50iwDtw6tHzpBa04oghna8tLvYkKrO5oAmKs5UODrOoBBBtBIcBq0WJkWZ8btrp\r\nA5pl9OBhRlLeNKbXeYfevqC03d4x1nTX9QhT9pffNdXLckcUn99i1euug4o3M441\r\nhXEfekt3JNiTsNRV47BjoHE0JIM/FzLcKnHG9+roHC9PB1ddn4ubMW2Qkx67dTC7\r\nvhXa3LOjfJoOVadWeIL7bshOMJBkIMqgpYqzqHSep7HUDpPywMlM504HNxLW2IUE\r\nyYtUqbKGrPuwXhGxiuNl+xEtSKcPJ/MQwhPNUE7si3wEZ+4QO+g04tc8m8M5B13D\r\nEbwEvHgJgrYu8w80Nkap0JMIjeAE/+wy3dfcLEPtNH208LuB0oFZrS6o2bNjsGPb\r\np3b2PJWQDs3JJI17jVVKoJj3xQi5esWYWTNrFZ2kLsAjkoXWUnrth1FSJ8wRTYch\r\n7e4gdrZa2IOzcokgTlJc7VFu7sy3rb/LFdRpkKDJCm6bZiORjjvcSr/iM442/ePl\r\nPOz/Wkx9IHiZ/Ymn42P23fAlmu4FhbT+CGaE5il+NHuftb9A/wi2lI/LZSbQHeBQ\r\npBRbpG+LrToEF/f/UmEZdkV0I6agnexU5bgdq7O2QZ/LzAj4+8JmrWiIVG+2vC//\r\nq30pUOxmWnxCdxZfHZ6dTddd0UZ4ICPnWWvrKCaIKCZU6v3/GKsJMdieqaf0sdf8\r\n3qjMq88ieT/+qjFFuygPVKyW/ximfs74NoPL8DlNGpvdzb7d7to7ask10BrqOyQJ\r\n2OvqusW6r0ssKmDIrt9LvDMNFBv0MWLpNtSwCQxTWlPjqIJ5CcJDo8VSkw+xaRlm\r\nIKiwZHYaNtJtrY1sWQHpZHlBWCkIsEfue/5CIflrh0IZSpXDvcgNDcq90E15Y96m\r\nW0vr4NmV1azFTeJhbdDcPoyitj90aqKZiItQ+4ULptytwXQa2L9iH4vsWQIBxI6h\r\na/6E4DlKxuM6RVcgcNpT98VhV6IsMf9GPCuHL6LLES2+M763PgIWm/avaXV7fnkM\r\n4Gx4PCTLV7cVvf0DuzGwDAfQhQ==\r\n-----END ENCRYPTED PRIVATE KEY-----\r\n",
        "current_key_number": 4,
        "keys": [
            "VBmlPOAsIoAsQbEp+s3xa0iXYkAc9ehHpWfwkkDNvD8mn9Rdh2wIyyDU2n/85TFl4YQDYwCwpxsOqab3zoq72hJmsE+nUJJH4pxw9+oLh9KtN3P1fjtGjuufXhRr1+PxqvCN7+VXC442gQiSACm2Wl5DiFTUyrWTpqRmOMBrRvI4/VL9ECdLn9dp/jk3JOX9eUSi1FzzHxc2xYLf8wAM641CON1fOAOmPCDdnUdLEfaNOda3sOu4Lx29oBQGoFqXGLF3liI+C2dDC8ohM3L/u0FbjpT4IF+V80sGqeZ3tMGie3raijq+GD7VOb3ZpjdvD+Jvbz28/Rjam7zZVZTj7mDZd9l+mRZdnZtnL76igkW6lz+2/3KVwxYSDb4MHKRgZRovHNC+UQErewNjespRAJhMRDwCcYNmi5aW6W8d7TE+9EBZV740tKi3GQL+TtpZB7UFHutDBZDqtWD5RYpUolkTw/+eji3yZKwOtpSRCxlUqZGKMj/iDUFLIImjoUWfnqTnbhcOfArtTaPzS2Sc9THnOeEA8+X0hh2boZdlMLeVD9DD4f7HP4VWgSAv8vXZUybC0qELI7XTixm19gfeC1yRbv0AfXs3V5JjYDgJSVyGDcUuwVdmWNdpKMXI3Mgs46WE8waDuEBpgVRXoU4w0HLoPGvv/RFpRf3pqyke8ao=",
            "hLi9OJBcGhd0jChYDMasAn+eZ32rn5po9GevDYwf7UE0VT8MNbN7FyBTUQNiXzHiRasYW3EXCQieLb7ydJMMvhrbUYbi7kSIaW+tpBbwGSBw9WcvA/N9JSTdAUlqG+UJthKIfkLOwUfc5eBZOhp/f2OPv6kq4Rlw68+tQbVTPTKXgDpuSPiwE8FwKVICKGe2G2pg5AUgUlNp29kzriR1UUD/huT4CJbsP/aDhIStVtqv0L/a473ZLQpswFj6NKhA4Ofwb8opLLAhd2aQXu/l87veRIWHrO041cFZDLBb8f+vbKlr+q4QF05HCc76Vz2Qgk6wyo5sFPfCacmyKTvN48HHFfmEraalSZmh/dMmU3M5IAdkVDWvZHIHhQ3uUh8wZkR2pvmxk9vcagqx3H9W6Hpm1P3aA54apyIlpGRNoEi3somDgA3GQcA/E0GLvlwiOmr6Gt2/2hnik8ja1WNI9qtSPUSRS7Z2WyiggHWYMd6tGRUYskrqoi31TMO0Ya1aMcCrp7JSG92p5vltSydt4d1esSlFNBR25vhrPLp/IF1FH4h/c/rlvCFxnOX6Z1TrhldIZ4s4Rau4seU/cBhDMnyKDWgzPm5mOCEDPG18ZB2lSrOArou/5EpBOC5ajSlIAlsfHI0olYEOxFjDopsS5GtLJCc69ywp/zkcwlHrfQM=",
            "frdVcnJogRNpuoNy++56i7VhEpYMjUq9iOV7KAX9+/5+EON/ylo1H8HR4qtZhrR+vK8bqsbrZU+acYtkRpO4HPBU6tFLzyKTg3hK3cflQL8NSjHM66XusZATuKGifcvDqRMi1ojiUX+sgRfpl1w+ForNJgBlti5H9TKrLc4jg8qh5JVcF9rlnUED+ojGxONNcuQzOWFEMnOWYb5Qd3Cd4Ds8nBuQEQrIAvNTZL/pH5PZuYgBup9eXnr9u0U6CT2H8Ge7dMMwXSHUTqh5lHzKdK4uevXxo5jLZi+DWT4jGktWJfQ5S3wkI88TwTrj99gPpR03uNurDzsfIjD73zUPi6S4Ya1kQDVlw0FpO+z3RHKJASmUd9W3hRdPCTtsfrT37b6QSglxeBIKolCh9psFhG6MQxFWIVhxooVoA+/ZG84xmcGiUsyhZnwvE3KvcbQBolQ0C6iihdAD904SK4DKRcxKiYR+2hbVLuecW9ZU42yiDLpl7AbUj19zuH3wOPvXcoQhmDa+/yb4GNCaDY7Xm39+A1WjJITqAFEMKw8J4vPKLEUdORz2jp4/SeE8y+BVzOx3SdbHlqqvBTmskiSMIg3x9e/Cietcr0NqkNeTnN5CVUgl0GsDuom075kljgJwaavPRmiwIHvDB/vlvZWLfj5EaEgTB/bAs1JIbyqfsCo=",
            "YTF426q0AzHjUTJvZkr1yi1K6lR//WBS6M6Pul5ZRwCCbN5NLpG/zkRkGhP04VM0boutkxD6bG4IitW3pJmlvEUKKXpln9zqDtLcv342JmjlSfBWlm2bcAQ6KpNY7TD7ZLp/zpAT9bf1UrlKtefAkkmBeWMzK2aTJK9UnlUfKDLKbgzVt/D9htZ0Y6QG7BF1QFynzfaSCbz+r1j5kPFziy6Hr+9amA3bL/08RdXmE9LN0pkZokaIMdhtlvwEeRcJhYUa/DmfX8WyacN13mR/Q7bPvUvgrnbWx61EEZI/AikXp2e1NXJOab+BhGvRTNIg0iULj3rE4uR40q1W0eV/E5OkWUMmajpkXGvgWUpjaQvlbdDrmPkLB+USnC3pf7/4CRSDXbxWJ1e/zIJs0tQWU6pU3UD/j46Gr0espClQAGtM9OM7+5UqCQg6lr4f34GUgdR63EJytC8/JUsE00e1oeRrchwabL5gV8TUhalthuJypWHG1UuJ92bAVN8TCGruQmu1eeQ+UgS6aFeQq8cqzSxDGtu6I7ydkaaGSZ1+UablxKKUqRwwCXYtDnzaKzzHUnzYJIBlnptDSM7jUdpe1el/2FLYeaWzl+SzAhUOAaqaVZIE/H2Gysb1/20VAONZqAjb9ou2ktnR27gei+Geks5cOCxeFgY83Bzr2fU8L0Y=",
            "Nay9vFI95U9zEcORJzIqLSushp9gYWzdBm9gSv0xViXrdHfYbB7uDhTSNsWtq6eOcbwHcoFQHV7UJkSemMUoeQ+UGp5KRPItHpFVFi34OUnWqEgwwDCZwudVoxD5gcdP/sN8oZKd5Ix9VPpPCZq/RZrORXp6L2JZ5GLvJk4Ok+/doVSD6beSeOzwvVaDlnsrmcgFMWFpYuFH7NAvVfEuJXQqOEFDHrp+zBfr1l2NOpGzCfDi99J8sKiqa1CAaVz5C1Yv5kM6AUmq6p41ApXq1wC56HjcEv85s/aauUXirqYkts+/BYOHm8ljKegPCoWpT7GDcgH0yYG2Wf11VLQUBcqOLGFCZInpqi3isTMNp2apcUTvgg7xjXFkkLXTtZjEqfLnwxKFgpU6EKazr9GqVmAyRufIcxLnXiY+aCCT7IsW0HrEdiujA3rIjsZHUBCl6JTMY1Exbz9IeN2S/9cQ4hvgnWQRgqyAy0/pveatZooEX+FzDIyzkh9UvOgddwp/OHwwUxmzItbz9LaYZFkl663i16GJWCFvx+bXJPHTRK5sjypG0lQFbe/TbuqhBfXhEb3YO4KFYjXVeMh6TiK93jyYKY/UbaKspdd5O37kL+k++jFj2FTU9nD5u4uqIsKyVf0pLR/jzRihVUXKLawgUgZeXO3E6gFKYyVCShrdgyM=",
            "FGogwREW7//CXQ5/1m8fKIdOqvYPGwNWudPJBiywLl8ocgpIKDEMr76DwHW9ZSJR5uqd09G3Gc5oD+jSN3Gxt7j0lGs8xSOLcB2xTAgeEUeBcZ5Di9CVdReVT0Jovr/+t/I5/Xiq96CwpQ4+0qeXAczJZ0HVbyvheXCi+Y+KpRr5/nC5xBKAnQL350j9RRtsMtlTlhqDHsBAzO3LZPGH75jiMcig6yGW5fV4wTd+5Su4Tcj6vh90UqNwEcQrELzsa/XN41U0ypmouR+luPFPVHuFsvTNvzGKlCnDI4wwYTvsYvSC3G27N+9CYC2YMtQNW7iM0HWimPAPj8S0BhrYa0znFpALE+epo/insJsRjn8c5KKwnTtaCGFf9BIqH2vRFLZAptZzxq4dc5+blIX67db93geho91Ocjgs4zvqJ0stgVY1k+SYCMqOGOkLNO3fuqR/fAkKH9xDUMGKx6NeGv8rj6Fwz50Emv7Y0XbDkw1FyC6xi4rMeehTV86/3w6qZ0Mzt5TKEEoHmgrknUSNypXEcMBS2BYVOYhGY0ufy0MLhTbxTN5Uup3jXqFEcDBF78x7JEg+7F8iJin3qxtj/9jfDgO4rNkuM9MT0n9aCx0IdHgmSzVolK2xjGV+c2u70szHVWbNqU2IyRJJG9GUqhx+Muv4wZgLfGWYsfhRprU=",
            "DC5/HMVdflNee1kEfn7HmGD4Z97WeLZglGUqrErn2cw2z9AxnYf0YXLyfgEfDMMtGNrf7c9TWQJ2WGUv2XlxuNhj3o264s/teGovUPmV/VrYpJZWW/MQCYqjKligS+yzh3sb/WLfAIuhVewEcNBNuoNqb5lcM4dPaP2dK2JYROOhAqZWZ9Z3oRT6ImyuRNVy/vZhvuo/vfoNHPtYWQ4SfMhpLIgFMuQ1s3ZWqjqlBQfA96POSLd3ReKXOEj4Hg2DN6tfWEcGVCye/WM9BwD1vezxnqZkt6crW4KWjFQJm891Mrys5f8Vf/12fX3kKhGB5OwUGJLy6w5PhyrxtMZnPS2FCsCpePeOtGbwJKSkq1UkXrSd0lTLD3PwtfoKAuJi1n9rbVLVUq7aAIbNL7itSlJgJbkgiYsotDaMSPr4PP1hEhEBZ/aM6jcELWTBqjfFMbcq6scvhHOmgbqoKgN/sLpoV5TimA4p/kogb8Kepsg40keBGiBU634/30ldaY3TCYkYiAIHBPH6BAByggzORE1bzeObnpnq31TEZbsREXUao+70NG1y6tRYkKP9uUBLjlmIwmCPsb0uSRYLr1IQwMiXEGwu2o7/BxKekouF1DjtBoVw6pgmbp4OVBNCNNyKJ3SMMfWADAknnixmG//Ppm9T/Dn4hwzH8Q/XQnAfGmY=",
            "VhJFzykcjyHt6uXyPuHCjL3K9PAl9TjyqD1QHTWk4imdurTxk0czo3U8N4QLGHDJxgxjeEHGuqu3an0jw4wbWz+CuHphAfUN9DacD8Ges5ZAi14juJz6E5YEXKf6vO28S/XwE3L3V0ikge0TouS2qalrdRd4vI68EBOxXP8/sozh/X3jd/y7O6oHoIZWUwbGbfKiWWJLxhSGiLfBnX5Rmx4EgolkRU0YIBES2D19u6NTDJuV5CNELBxU4s+0rLOybHuAxhBSF3OnEASwxYt9Q7BLl7jNsOGBBgUtsrFBxLQpxJKgXlW3iiRsnx+I6UJ7dL1w2KpA48vXSJdJnaXHSNmO2tkSY5wmbXGAGlKW5LMDWnTf9Lti/wOqcBSruQZJcP957r4c5LqhSkFu0etoYPvcdthOVpHWCnW/TAV6qObRXp49ih3kkCtUnguBgbFC0sLPAMzZ2Ei7+hQhfMqTbImfvvxkO3DdF/8Qd4zy9a69M5h5uv72xKpIsz+oUo4I30pomDhBEY8/LMmFgp5LyIqf6lCh7JrCtCMhxxiFXpCwwCmJ+yDLaIkCoyirvCWopmsSh5kaiTKekpq1ZrLr7YVPghGEEOl67WD3Ir4FCNVM+k3d9jKq0h1mcwkLdKhaGBKhEqMIY/0UQdUHH1uJZn5GTNwDFSucBEKNSzSzRIE=",
            "g33ZxWIc0GQxjHAaBIHlwp5n8q0x/DfvSafqFdKTo5mh8gKl0BB187EkMJW67msBLy2Lndml6FKyXdmjCfkVFu1v7PwECHRD1rE1zIs7yyXNrPvZGRJtuzn8+guRefWKjeUC8ALtkpEaXuyEirJjdIk1SvZghowRjbmXuCTpX1O7+zFHiET7s8y5utIu6jSQ6iqpP+AAo9MaSNHypyprAh9lSGF+x+Gvqq4NhK4pceTKrBdFygEXDCI0SfWT8pB1X24yNL4Mi3cW0td0PRDjPW8eXdze1BPY06w3tzgVBY+jUJSxPGTQfHDxaou4seyVR++orVzzwlYKxc5GXoK31ovugE7/VtZ42TDRPUC3ZWO6UU9c1/A3u+chkwKshAVRlng3tk03+1IcGxWazyAB19OBdxmmR4eeyt76TSkgykC1Em0PwZd0m0qMaoDRXM5LJaBgPIX9cFQKvlvwzTOrxk5Ak5UvtQTeIHaEoDTXBp3Ostv4RTwToAE+640G8Z1d1tANTXM0olKCEjfP6MRtecFqYR2thSeAZ38ZLOdyTIMfnQ+IlNY5LwOi5Z1KsHBMWS5AArRS7SjezxzIMmM8GF7F/zglxRtaZ6RSttotvcFZ4vPzkAJzRYTlv6hIMoM5C7fOy2+E+Dowcz+TkvjHo0hgL2ED7RRTW+cSxemGtPA="
        ]
    }
}

encrypted_strings = [
    '000-01L-XG3y',
    '100-01f-FJ83',
    '200-06F-Fm2Z',
    '300-08n-j4vC',
    '400-07i-HT5O',
    '500-0aD-91vi',
    '600-02y-zoUg',
    '700-08m-LP44',
    '800-08x-Q6Ek',
]

# for idx in range(0,9):
#     cache[ffs_name]['current_key_number'] = idx
#     enc = fpe.EncryptCache('LamjC19/C1t0ZGdlnt9PclRGefC1o2zNlJfzSC/5bwDr', ffs_name, cache, '123-456-7890')
#     dec = fpe.DecryptCache('LamjC19/C1t0ZGdlnt9PclRGefC1o2zNlJfzSC/5bwDr', ffs_name, cache, enc)
#     print("%d - Encrypted: %s Decrypted %s" %(idx, enc, dec))

# print("Current key number %d" %(cache[ffs_name]['current_key_number']))
# enc = fpe.EncryptCache('LamjC19/C1t0ZGdlnt9PclRGefC1o2zNlJfzSC/5bwDr', ffs_name, cache, '123-456-7890')
# print("encrypted %s" %(enc))
# dec = fpe.DecryptCache('LamjC19/C1t0ZGdlnt9PclRGefC1o2zNlJfzSC/5bwDr', ffs_name, cache, enc)
# print("decrypted %s" %(dec))

fmt, trm = fmtInput('123a456-7890', '-', '1234567890', '1234567890abcdefghijklmnopqrstuvwxyz')
print("fmt %s trm %s" %(fmt, trm))